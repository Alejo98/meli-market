# Documentación del Proyecto: Meli Market

## Descripción

Este proyecto, desarrollado por **Alejandro Vallejo Parra**, representa una plataforma de comercio en línea basada en Spring Boot. A continuación, se presenta una visión general de la arquitectura, funcionalidades clave y la estructura del proyecto.

## Resumen del Proyecto

Meli Market es una aplicación de comercio electrónico que permite a los usuarios navegar, buscar y comprar productos en línea. La plataforma proporciona una serie de características esenciales para garantizar una experiencia de compra satisfactoria. A continuación, se destacan algunas de las principales funcionalidades:

- Gestión de productos: Los administradores pueden agregar, editar y eliminar productos en el catálogo.
- Gestión de compras: Los usuarios pueden realizar compras, revisar su historial y ver detalles de las transacciones.
- Búsqueda de productos: Los clientes pueden buscar productos por nombre, categoría y otras características.
- Documentación interactiva: La API del proyecto se documenta mediante Swagger para facilitar su uso y pruebas.

## Estructura del Proyecto

El proyecto se organiza en capas y paquetes para garantizar una estructura clara y mantenible:

- **Controller:** Este paquete contiene los controladores que gestionan las solicitudes HTTP y definen las rutas de acceso a las funcionalidades.

- **Service:** Los servicios implementan la lógica de negocio y coordinan la interacción con la capa de persistencia.

- **Persistence:** Incluye las entidades JPA que representan las tablas de la base de datos y los repositorios para acceder a los datos.

- **Mapper:** Los mappers se utilizan para convertir objetos de dominio en objetos de entidad y viceversa.

- **Domain:** Aquí se definen las clases de dominio que representan los objetos centrales de la aplicación.

## Endpoints Destacados

A continuación, se presentan algunos de los endpoints disponibles en el proyecto:

- **GET /products/all:** Recupera todos los productos disponibles en el mercado.
- **GET /products/{id}:** Busca un producto por su ID.
- **POST /products/save:** Registra un nuevo producto en la base de datos.
- **GET /purchases/all:** Obtiene todas las compras registradas en el sistema.
- **GET /purchases/client/{idClient}:** Recupera las compras realizadas por un cliente específico.
- **POST /purchases/save:** Registra una nueva compra.

## Tecnologías Utilizadas

Este proyecto se basa en una serie de tecnologías y bibliotecas clave:

- Spring Boot: Framework de desarrollo de aplicaciones Java.
- Spring Data JPA: Para el acceso a la base de datos.
- Spring Web: Para crear servicios web RESTful.
- Swagger: Para documentación y prueba de la API.
- MapStruct: Para mapeo de objetos.
- Base de datos Postgresql

## Estructura del Proyecto: build.gradle

El archivo `build.gradle` del proyecto incluye las dependencias y plugins utilizados. Aquí se muestra un fragmento relevante del archivo `build.gradle`:

```groovy
plugins {
    id 'java'
    id 'org.springframework.boot' version '2.7.16'
    id 'io.spring.dependency-management' version '1.0.15.RELEASE'
}

dependencies {
    implementation 'org.springframework.boot:spring-boot-starter-data-jpa'
    implementation 'org.springframework.boot:spring-boot-starter-web'
    implementation 'io.springfox:springfox-swagger2:2.9.2'
    implementation 'io.springfox:springfox-swagger-ui:2.9.2'
    runtimeOnly 'org.postgresql:postgresql'
    implementation 'org.mapstruct:mapstruct:1.5.5.Final'
    annotationProcessor 'org.mapstruct:mapstruct-processor:1.5.5.Final'
}
